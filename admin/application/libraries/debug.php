<?php

defined('BASEPATH') or exit('No direct script access allowed');

class debug
{
    public function __construct()
    {
    }
    public function dd($data)
    {
        echo "<pre>";
        echo $data;
        echo "</pre>";
        die();
    }
}
