<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dash extends CI_Controller
{
	public function index()
	{
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('dash/index');
		$this->load->view('includes/footer');
		$this->load->view('includes/scripts');
	}
}
