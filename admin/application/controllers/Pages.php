<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('PagesModel');
	}

	public function index($message = null)
	{
		$data['pages'] = $this->PagesModel->all();
		$data['message'] = $message;

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('pages/index', $data);
		$this->load->view('includes/footer');
		$script['script'] = 'pages/custom_js';
		$this->load->view('includes/scripts', $script);
	}

	public function create($message = null)
	{
		$data['message'] = $message;

		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('pages/create', $data);
		$this->load->view('includes/footer');
		$script['script'] = 'pages/custom_js';
		$this->load->view('includes/scripts', $script);
	}

	public function create_function()
	{
		$this->form_validation->set_rules('page_title', 'Page Title', 'required|strip_tags|max_length[100]|trim|is_unique[pages_tbl.page_title]');
		$this->form_validation->set_rules('page_description', 'Page Description', 'required|strip_tags|trim');

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$message = $this->PagesModel->create($_POST);
			$this->index($message);
		}
	}

	public function edit()
	{
		$this->load->view('includes/header');
		$this->load->view('includes/navbar');
		$this->load->view('includes/sidebar');
		$this->load->view('pages/edit');
		$this->load->view('includes/footer');
		$script['script'] = 'pages/custom_js';
		$this->load->view('includes/scripts', $script);
	}

	public function delete_page()
	{
		$this->PagesModel->delete_page($_POST['page_id']);
	}
}
