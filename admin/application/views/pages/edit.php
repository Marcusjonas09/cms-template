 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <div class="content-header">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-6 d-flex align-items-center">
                     <a href="<?= base_url() ?>pages">
                         <h3 class="m-0 text-dark font-weight-bold "><i class="fas fa-chevron-left"></i>
                             Back</h3>
                     </a>
                 </div><!-- /.col -->
             </div><!-- /.row -->
         </div><!-- /.container-fluid -->
     </div>
     <!-- /.content-header -->

     <!-- Main content -->
     <section class="content">
         <div class="container-fluid">





             <div class="row">
                 <div class="col-md-6">
                     <div class="card mr-4">
                         <div class="card-header">
                             <h3 class="card-title font-weight-bold">Create Page</h3>
                         </div>
                         <form role="form">
                             <div class="card-body">
                                 <div class="form-group">
                                     <label for="page_title">Title</label>
                                     <input value="<?= set_value('page_title') ?>" type="email" class="form-control" name="page_title" id="page_title" placeholder="Enter page title">
                                 </div>
                                 <div class="form-group">
                                     <label for="exampleInputPassword1">Password</label>
                                     <textarea class="form-control" id="page_description" rows="5" placeholder="Enter page description"><?= set_value('page_description') ?></textarea>
                                 </div>
                             </div>
                             <div class="card-footer">
                                 <button type="submit" class="btn btn-primary col-md-3 float-right">Save</button>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>




         </div>
         <!--/. container-fluid -->
     </section>
     <!-- /.content -->
 </div>
 <!-- /.content-wrapper -->