 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <div class="content-header">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-6 d-flex align-items-center">
                     <a href="<?= base_url() ?>pages">
                         <h3 class="m-0 text-dark font-weight-bold "><i class="fas fa-chevron-left"></i>
                             Back</h3>
                     </a>
                 </div><!-- /.col -->
             </div><!-- /.row -->
         </div><!-- /.container-fluid -->
     </div>
     <!-- /.content-header -->

     <!-- Main content -->
     <section class="content">
         <div class="container-fluid">


             


             <div class="row">
                 <div class="col-md-6">
                     <div class="card mr-4">
                         <div class="card-header">
                             <h3 class="card-title font-weight-bold">Create Page</h3>
                         </div>
                         <form action="<?= base_url() ?>pages/create_function" method="POST">
                             <div class="card-body">
                                 <div class="form-group">
                                     <label for="page_title">Title</label>
                                     <?php echo form_error('page_title', '<p class="text-warning">', '</p>'); ?>
                                     <input type="text" class="form-control <?= form_error('page_title') ? 'is-warning' : ''; ?>" value="<?= set_value('page_title') ?>" name="page_title" id="page_title" placeholder="Enter page title">
                                 </div>
                                 <div class="form-group">
                                     <label for="page_description">Description</label>
                                     <?php echo form_error('page_description', '<p class="text-warning">', '</p>'); ?>
                                     <textarea class="form-control <?= form_error('page_description') ? 'is-warning' : ''; ?>" name="page_description" id="page_description" rows="5" placeholder="Enter page description"><?= set_value('page_description') ?></textarea>
                                 </div>
                             </div>
                             <div class="card-footer">
                                 <button type="submit" class="btn btn-primary col-md-3 float-right">Save</button>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>




         </div>
         <!--/. container-fluid -->
     </section>
     <!-- /.content -->
 </div>
 <!-- /.content-wrapper -->