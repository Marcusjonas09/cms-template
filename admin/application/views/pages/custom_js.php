<script>
    function delete_page(page_id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?= base_url() ?>pages/delete_page",
                    method: "POST",
                    data: {
                        page_id: page_id
                    },
                    success: function(data) {
                        Swal.fire(
                            'Deleted!',
                            'Page has been deleted.',
                            'success',
                        ).then((value) => {
                            location.href = "<?= base_url() ?>pages";
                        })
                    }
                });

            }
        })
    }

    $(document).ready(function() {
        // $(".table").DataTable();
        $('.table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
        });
    });
</script>