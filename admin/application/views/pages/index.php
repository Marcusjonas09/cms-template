 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <div class="content-header">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-sm-6 d-flex align-items-center">
                     <h3 class="m-0 text-dark font-weight-bold ">Pages &ensp;<a href="<?= base_url() ?>pages/create" class="btn btn-primary float-right">New Page</a></h3>
                 </div><!-- /.col -->
             </div><!-- /.row -->
         </div><!-- /.container-fluid -->
     </div>
     <!-- /.content-header -->

     <!-- Main content -->
     <section class="content">
         <div class="container-fluid">



             <div class="row">
                 <div class="col-md-12">
                     <?php if (isset($message)) {
                            echo $message;
                        } ?>
                 </div>
             </div>



             <div class="card">
                 <div class="card-body">
                     <table class="table table-bordered table-striped">
                         <thead>
                             <tr>
                                 <th style="width:5%;">#</th>
                                 <th style="width:25%;">Title</th>
                                 <th style="width:60%;">Description</th>
                                 <th style="width:10%;">Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             <?php $i = 1;
                                foreach ($pages as $page) : ?>
                                 <tr>
                                     <td><?= $i++ ?></td>
                                     <td><?= $page->page_title ?></td>
                                     <td><?= word_limiter($page->page_description, 10, ' &hellip;') ?></td>
                                     <td>
                                         <button class="btn btn-primary btn-sm"><i class="fas fa-eye text-light"></i></button>
                                         <button class="btn btn-danger btn-sm" onclick="delete_page(<?= $page->page_id ?>)"><i class="fas fa-trash nav-icon"></i></button>
                                     </td>
                                 </tr>
                             <?php endforeach; ?>
                         </tbody>
                     </table>
                 </div>
                 <!-- /.card-body -->
             </div>
             <!-- /.card -->










         </div>
         <!--/. container-fluid -->
     </section>
     <!-- /.content -->
 </div>
 <!-- /.content-wrapper -->