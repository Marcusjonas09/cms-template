<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; <?= date('Y', time()) ?> <a href="#">CURIOSO</a>.</strong>
    <div class="float-right d-none d-sm-inline-block">
        All rights reserved.
    </div>
</footer>
</div>
<!-- ./wrapper -->