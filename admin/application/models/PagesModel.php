<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PagesModel extends CI_Model
{
    public function all()
    {
        return $this->db->get('pages_tbl')->result();
    }

    public function create($page_details)
    {
        $this->db->insert('pages_tbl', $page_details);
        $message = '
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-check"></i> Success!</h5>
            Page created successfuly!
        </div>
        ';
        return $message;
    }

    public function delete_page($page_id)
    {
        $this->db->delete('pages_tbl', array('page_id' => $page_id));
    }
}
